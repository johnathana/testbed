package com.company.scheduler;

import org.zkoss.bind.annotation.Command;

import static org.zkoss.zk.ui.util.Clients.showNotification;

public class BoardViewModel {

    @Command
    public void startGetNewsJob() {
        //launch job epant_get_news on demand and return

        showNotification("I am the server, you just poked me!", true);
    }
}
